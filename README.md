# лабораторные работу по курсу вычислительной математики
## Подготовка

выполнить команду
```bash
$ git clone https://gitlab.com/Niles_Penrose/prep_for_math.git
```


ТРЕБОВАНИЯ
    pytnon3.8+,
    python3-venv
```bash
$ sudo apt-get install python3
```
```bash
$ sudo apt-get install python3-venv
```
1) Установить venv
```bash
$ python3 -m venv venv
```

2) Активировать venv
```bash
$ . venv/bin/activate
```


установить всё что необходимо для запуска проекта
```bash
$ pip install -r requirements.txt

Collecting aiohttp==3.7.3
  Using cached aiohttp-3.7.3-cp38-cp38-manylinux2014_x86_64.whl (1.5 MB)
Collecting async-timeout==3.0.1
  Using cached async_timeout-3.0.1-py3-none-any.whl (8.2 kB)
Collecting attrs==20.3.0
  Using cached attrs-20.3.0-py2.py3-none-any.whl (49 kB)
Collecting chardet==3.0.4
  Using cached chardet-3.0.4-py2.py3-none-any.whl (133 kB)
Collecting idna==3.1
  Using cached idna-3.1-py3-none-any.whl (58 kB)
Collecting multidict==5.1.0
  Using cached multidict-5.1.0-cp38-cp38-manylinux2014_x86_64.whl (159 kB)
Collecting typing-extensions==3.7.4.3
  Using cached typing_extensions-3.7.4.3-py3-none-any.whl (22 kB)
Collecting yarl==1.6.3
  Using cached yarl-1.6.3-cp38-cp38-manylinux2014_x86_64.whl (324 kB)
Installing collected packages: chardet, async-timeout, multidict, typing-extensions, idna, yarl, attrs, aiohttp
Successfully installed aiohttp-3.7.3 async-timeout-3.0.1 attrs-20.3.0 chardet-3.0.4 idna-3.1 multidict-5.1.0 typing-extensions-3.7.4.3 yarl-1.6.3

```

запустить скрипт

```bash
$ python main.py
======== Running on http://0.0.0.0:8080 ========
(Press CTRL+C to quit)
```

в случае необходимости деактивировать venv
```bash
$ deactivate
```


